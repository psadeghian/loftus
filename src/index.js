import React from 'react';
import ReactDOM from 'react-dom';
import {ApolloLink,  HttpLink, InMemoryCache, ApolloClient } from 'apollo-boost'
import { ApolloProvider } from '@apollo/react-hooks';
import App from './components/App';
import * as serviceWorker from './serviceWorker';
import { AUTH_TOKEN, GRAPHQL_ENDPOINT } from "./constants/constant";

const httpLink = new HttpLink({uri: GRAPHQL_ENDPOINT});

const middlewareLink = new ApolloLink((operation, forward) => {
  // get the authentication token from local storage if it exists
  const tokenValue = localStorage.getItem(AUTH_TOKEN);
  // return the headers to the context so httpLink can read them
  operation.setContext({
    headers: {
      Authorization: tokenValue ? `Bearer ${tokenValue}` : '',
    },
  });
  return forward(operation)
});

// authenticated httplink
const httpLinkAuth = middlewareLink.concat(httpLink);

// apollo client setup
const client = new ApolloClient({
  link: httpLinkAuth,
  cache: new InMemoryCache(),
  connectToDevTools: true,
});

ReactDOM.render(
  <ApolloProvider client={client}>
    <App/>
  </ApolloProvider>
  , document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

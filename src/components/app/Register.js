import React from 'react';
import TextField from "@material-ui/core/TextField/TextField";
import Button from "@material-ui/core/Button/Button";
import { makeStyles } from '@material-ui/core/styles';
import { gql } from 'apollo-boost';
import { useMutation } from '@apollo/react-hooks';
import * as uuid from "uuid";

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200,
  },
  button: {
    margin: theme.spacing(1),
    width: 200,
  },
}));

function Register() {
  const classes = useStyles();
  const [registerUser, {data}] = useMutation(REGISTER_USER);

  const handleClick = () => {
    const firstName = document.getElementById('firstName').value;
    const lastName = document.getElementById('lastName').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const phoneNumber = document.getElementById('phoneNumber').value;
    const userId = uuid.v4();
    registerUser({
      firstName, lastName, email, password, phoneNumber, uuid: userId
    })
  };

  return (
    <>
      <p>
        {data ? data.stringify() : ''}
      </p>
      <TextField
        id="firstName"
        className={classes.textField}
        label="first name"
        margin="normal"
        variant="filled"
        type="text"
        required={true}
      />
      <TextField
        id="lastName"
        className={classes.textField}
        label="last name"
        margin="normal"
        variant="filled"
        type="text"
        required={true}
      />
      <TextField
        id="email"
        className={classes.textField}
        label="email"
        margin="normal"
        variant="filled"
        type="email"
        required={true}
      />
      <TextField
        id="password"
        className={classes.textField}
        label="password"
        margin="normal"
        variant="filled"
        type="password"
        required={true}
      />
      <TextField
        id="phoneNumber"
        className={classes.textField}
        label="phone number"
        margin="normal"
        variant="filled"
        type="phone"
        required={true}
      />
      <Button variant="contained" color="primary" className={classes.button} onClick={handleClick}>
        Register
      </Button>
    </>
  )
}

const REGISTER_USER = gql`
    mutation RegisterUser($input: registerUserInput!) {
        registerUser(input: $input) {
            clientMutationId
        }
    }
`;

export default Register;
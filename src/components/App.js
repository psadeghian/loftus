import React from 'react';
import '../App.css';
import Register from "./app/Register";

//firstName: "Bruce", lastName: "Wayne", email: "brucewayne@gmail.com", password: "brucewayne", phoneNumber: "18433211213", uuid: "a0fc219a-282f-4fcc-acfc-4120f66098f3"

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Register/>
      </header>
    </div>
  )
}

export default App;
